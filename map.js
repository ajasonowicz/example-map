function init() {
  var bounds = null;

  var map = L.map(
    'map', {
      center: [50, -145], 
      zoom: 4,
      maxBounds: bounds,
      layers: [],
      worldCopyJump: false,
      crs: L.CRS.EPSG3857
    });



  var tile_layer_EsriOceanBasemap = L.tileLayer(
    'http://server.arcgisonline.com/ArcGIS/rest/services/Ocean_Basemap/MapServer/tile/{z}/{y}/{x}', {
      "attribution": "Tiles &copy; Esri &mdash; Sources: GEBCO, NOAA, CHS, OSU, UNH, CSUMB, National Geographic, DeLorme, NAVTEQ, and Esri",
      "detectRetina": false,
      "maxZoom": 20,
      "minZoom": 1,
      "noWrap": false,
      "subdomains": "abc"
    }
  ).addTo(map);

  var tile_layer_OpenMapSurfer = L.tileLayer(
    'https://maps.heigit.org/openmapsurfer/tiles/roads/webmercator/{z}/{x}/{y}.png', {
      maxZoom: 20,
      minZoom: 1,
      continuousWorld: false,
      noWrap: false,
      attribution: 'Imagery from <a href="http://giscience.uni-hd.de/">GIScience Research Group @ University of Heidelberg</a> | Map data &copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors',
      detectRetina: true
    }
  );

  var polygon1 = L.polygon(
    [
      [57.80536027997975, -165.2561806141216],
      [57.22060262467294, -164.4810920953492],
      [56.24472609279501, -161.7163912849859],
      [56.87481974150633, -159.840757787318],
      [57.59996278051455, -159.0446088346201],
      [58.20178318424947, -158.5102451185562],
      [58.569020865714, -160.3124988585108],
      [58.44815752247688, -162.9694760169474],
      [57.80536027997975, -165.2561806141216]
    ], {
      color: "#EB6841",
      fillOpacity: 0.65,
      weight: 1
    }).addTo(map);
  var pd1 = L.polylineDecorator(polygon1, {
    patterns: [{
      offset: 0,
      repeat: 10,
      symbol: L.Symbol.dash({
        pixelSize: 0
      })
    }]
  }).addTo(map);

  polygon1.bindPopup("<h4>Nursery Area</h4>Example nursery area.")

  var polygon2 = L.polygon(
    [
      [57.75880579362964, -149.8567279141318],
      [57.14032044272003, -151.0311929286624],
      [56.6230929882964, -150.5699470846407],
      [56.85918826494344, -148.6875862110461],
      [57.84144490928968, -148.191094462657],
      [57.75880579362964, -149.8567279141318]
    ], {
      color: "#EDC951",
      fillOpacity: 0.65,
      weight: 1
    }).addTo(map);
  var pd2 = L.polylineDecorator(polygon2, {
    patterns: [{
      offset: 0,
      repeat: 10,
      symbol: L.Symbol.dash({
        pixelSize: 0
      })
    }]
  }).addTo(map);

  polygon2.bindPopup("<h4>Spawing Area</h4>Example spawning area.")

  var coords1 = [
    [48.54872134744678, -126.4882115552417],
    [49.13313313648913, -127.4008936319652],
    [49.8758237712496, -128.3624728578392],
    [50.41531971059461, -129.5716548742551],
    [51.10281449874933, -130.4769405105316],
    [51.95553451917796, -131.8338972204298],
    [52.63072940015649, -132.6938962731748],
    [53.27070131770762, -133.5830933610231],
    [53.94686839839963, -134.1641896961531],
    [54.86785000671652, -134.6642256877881],
    [55.83524381119943, -135.4390186007744],
    [56.92216200985634, -136.2146177907996],
    [57.55841966240648, -136.8600673612945],
    [57.92723693600714, -138.2769640435031],
    [58.12436939586112, -139.1049659454045],
    [58.34913953433779, -140.2262057341671],
    [58.75742569841463, -141.3624684350507]
  ]
  var coords2 = [[60.18822640746222, -179.3013654689972],
  [59.3116646902655, -177.5435678307204],
  [58.64936602831096, -175.7601431694803],
  [57.91040688567035, -174.3015779862546],
  [57.6278302261877, -173.9337136206812],
  [56.97888131405236, -172.270631839822],
  [56.54560911482815, -170.9419625200831],
  [56.40639462566166, -170.6539305981475],
  [55.76376252904777, -169.3182317639712],
  [54.96889706561461, -168.2845680728276],
  [54.05933386854279, -167.9477993646765],
  [53.3185869828606, -170.3810130182748],
  [53.2195201557721, -170.9160222824192],
  [52.61810809820936, -173.7910001058817],
  [52.27249874334294, -177.209143124819],
  [52.16243683862386, -179.6740025743333],
  [52.3833484611526, -182.461659810369]]
  var poly1 = L.polyline(coords1, {
    color: "#CC333F",
    weight: 5,
    opacity: 0.6
  }).addTo(map);

  L.polylineDecorator(poly1, {
    patterns: [{
      offset: 0,
      repeat: 50,
      symbol: L.Symbol.arrowHead({
        pixelSize: 15,
        pathOptions: {
          color: "#CC333F",
          fillOpacity: 1,
          weight: 0
        }
      })
    }]
  }).addTo(map);

  poly1.bindPopup("<h4>Migration Path</h4>Example migration path 1.")

  var poly2 = L.polyline(coords2, {
    color: "#CC333F",
    weight: 5,
    opacity: 0.6
  }).addTo(map);

  L.polylineDecorator(poly2, {
    patterns: [{
      offset: 0,
      repeat: 50,
      symbol: L.Symbol.arrowHead({
        pixelSize: 15,
        pathOptions: {
          color: "#CC333F",
          fillOpacity: 1,
          weight: 0
        }
      })
    }]
  }).addTo(map);

  poly2.bindPopup("<h4>Migration Path</h4>Example migration path 2.")

  var basemap_layer_control = {
    base_layers: {
      "Esri Ocean Basemap": tile_layer_EsriOceanBasemap,
      "Open Map Surfer (Roads)": tile_layer_OpenMapSurfer,
    },

  };

  L.control.layers(
    basemap_layer_control.base_layers,
    basemap_layer_control.overlays, {
      position: 'topright',
      collapsed: true,
      autoZIndex: true
    }).addTo(map);



  var measureControl = new L.Control.Measure({
    primaryLengthUnit: 'miles',
    secondaryLengthUnit: 'kilometers',
    primaryAreaUnit: 'sqmiles',
    secondaryAreaUnit: undefined,
    activeColor: '#ABE67E',
    completedColor: '#000'
  });
  measureControl.addTo(map);

}